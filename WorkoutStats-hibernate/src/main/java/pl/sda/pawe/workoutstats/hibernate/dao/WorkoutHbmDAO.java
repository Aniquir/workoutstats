package pl.sda.pawe.workoutstats.hibernate.dao;

import com.sun.xml.internal.ws.api.pipe.FiberContextSwitchInterceptor;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import pl.sda.pawe.domain.entity.Workout;

import java.util.List;

@Repository
public class WorkoutHbmDAO {

    private final SessionFactory sessionFactory;

    @Autowired
    public WorkoutHbmDAO(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public Long create(Workout workout){
        Session session = sessionFactory.getCurrentSession();
        Long workoutId = (Long) session.save(workout);

        return workoutId;
    }
    public void update(Workout workout) {
        Session session = sessionFactory.getCurrentSession();
        session.update(workout);
    }
    public List<Workout> findAll() {
        Session session = sessionFactory.getCurrentSession();
        List<Workout> workoutsStorage = session.createQuery("FROM Workout", Workout.class).getResultList();

        return workoutsStorage;
    }
    public Workout findById(Long id) {
        Session session = sessionFactory.getCurrentSession();
        Workout workout = session.get(Workout.class, id);

        return workout;
    }
    public Workout merge(Workout workout) {
        Session session = sessionFactory.getCurrentSession();
        Workout mergedWorkout = (Workout) session.merge(workout);

        return mergedWorkout;
    }

    public void delete(Workout workout){
        Session session = sessionFactory.getCurrentSession();
        session.delete(workout);
    }

}
