package pl.sda.pawe.workoutstats.hibernate.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.sda.pawe.domain.entity.Workout;

@Repository
public interface WorkoutRepository extends JpaRepository<Workout, Long> {
}
