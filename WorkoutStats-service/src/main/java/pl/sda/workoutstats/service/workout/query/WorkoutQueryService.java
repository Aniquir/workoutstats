package pl.sda.workoutstats.service.workout.query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.sda.pawe.domain.entity.Workout;
import pl.sda.pawe.workoutstats.jdbc.dao.WorkoutJdbcDAO;
import pl.sda.workoutstats.service.workout.exception.WorkoutNotFoundException;


import java.util.List;

@Service
@Transactional
public class WorkoutQueryService {

    private static final Logger LOGGER = LoggerFactory.getLogger(WorkoutQueryService.class);

    private final WorkoutJdbcDAO workoutJdbcDAO;

    @Autowired
    public WorkoutQueryService(WorkoutJdbcDAO workoutJdbcDAO) {
        this.workoutJdbcDAO = workoutJdbcDAO;
    }

    public List<Workout> findAll(){
        return workoutJdbcDAO.findAll();
    }

    public Workout findById(Long id) {
        Workout workout = workoutJdbcDAO.findById(id);
        if (workout == null) {
            LOGGER.debug(" - Workout with id " + id + " not found - ");
            throw new WorkoutNotFoundException();
        }

        return workout;
    }
}
