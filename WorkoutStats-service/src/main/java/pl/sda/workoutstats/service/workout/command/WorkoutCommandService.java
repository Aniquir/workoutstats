package pl.sda.workoutstats.service.workout.command;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.sda.pawe.domain.entity.Workout;
import pl.sda.pawe.workoutstats.hibernate.repository.WorkoutRepository;
import pl.sda.workoutstats.service.workout.exception.WorkoutNotFoundException;

@Service
@Transactional
public class WorkoutCommandService {

    private static final Logger LOGGER = LoggerFactory.getLogger(WorkoutCommandService.class);

    private final WorkoutRepository workoutRepository;

    @Autowired
    public WorkoutCommandService(WorkoutRepository workoutRepository) {
        this.workoutRepository = workoutRepository;
    }
    public Long create(Workout workout){
        workoutRepository.save(workout);

        return workout.getId();
    }

    public void update(Workout workout){
        Workout dbWorkout = workoutRepository.findOne(workout.getId());
        if (dbWorkout == null){
            LOGGER.debug(" - Workout with id " + workout.getId() + " not found - ");
            throw new WorkoutNotFoundException();
        }
        dbWorkout.setDateW(workout.getDateW());
        dbWorkout.setTypeOfExcercise(workout.getTypeOfExcercise());
        dbWorkout.setSeries(workout.getSeries());
        dbWorkout.setSeries2(workout.getSeries2());
        dbWorkout.setSeries3(workout.getSeries3());
        dbWorkout.setNumberOfRepetitions(workout.getNumberOfRepetitions());
        dbWorkout.setNumberOfRepetitions2(workout.getNumberOfRepetitions2());
        dbWorkout.setNumberOfRepetitions3(workout.getNumberOfRepetitions3());
    }

    public void delete(Long id){
        Workout workout = workoutRepository.findOne(id);
        if (workout == null){
            LOGGER.debug(" - Workout with id " + id + " not found. - ");
            throw new WorkoutNotFoundException();
        }

        workoutRepository.delete(workout);
    }

}
