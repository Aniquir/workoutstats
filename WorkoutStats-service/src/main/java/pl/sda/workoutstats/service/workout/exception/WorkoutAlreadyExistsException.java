package pl.sda.workoutstats.service.workout.exception;


public class WorkoutAlreadyExistsException extends RuntimeException{
    private static final long serialVersionUID = 3049061904553145862L;
}
