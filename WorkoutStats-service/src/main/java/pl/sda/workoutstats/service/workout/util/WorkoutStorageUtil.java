package pl.sda.workoutstats.service.workout.util;

import common.util.DateUtil;
import pl.sda.pawe.domain.entity.Workout;

import java.time.LocalDate;
import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;

public class WorkoutStorageUtil {

    public static ConcurrentHashMap<Long, Workout> workoutsStorage = new ConcurrentHashMap<>();

    static {
        workoutsStorage.put(1L, createWorkout(1L, DateUtil.convertToDate(LocalDate.of(2017, 11, 10)), "Brzuszki", 1L, 2L, 3L, 5L, 10L, 15L));
    }

    private static Workout createWorkout(Long id, Date dateW, String typeOfExcercise,
                                         Long series, Long series2, Long series3,
                                         Long numberOfRepetitions, Long numberOfRepetitions2,
                                         Long numberOfRepetitions3){
        Workout workout = new Workout(dateW, typeOfExcercise, series, series2, series3,
                numberOfRepetitions, numberOfRepetitions2, numberOfRepetitions3);
        workout.setId(id);

        return workout;
    }
}

