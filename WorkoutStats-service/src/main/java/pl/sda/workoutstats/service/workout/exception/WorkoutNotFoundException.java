package pl.sda.workoutstats.service.workout.exception;

public class WorkoutNotFoundException extends RuntimeException {
    private static final long serialVersionUID = 4710778181311702822L;
}
