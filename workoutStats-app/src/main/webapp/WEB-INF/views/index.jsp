<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="code" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>- Workout Stats -</title>
    <%--css--%>
    <spring:url value="/resources/theme1/css/main.css" var="mainCss" />
    <link href="${mainCss}" rel="stylesheet">

</head>

<body>

<spring:message code="lang.change"/>
<a href="?language=en"><spring:message code="lang.eng"/></a>
<a href="?language=pl"><spring:message code="lang.pl"/></a>


<c:url var="saveStats" value="/workout/save"/>
<div>
<table>
    <tr>
        <th colspan="2" align="center"><spring:message code="app.title"/></th>
    </tr>
    <tr>
    <td valign="top">
<form:form method="post" modelAttribute="workout" action="${saveStats}">
    <table>
        <tr>
            <h3><spring:message code="workoutstats.form.title"/></h3>
        </tr>
        <tr>
            <%--colspan jest tu v nie potrzebny, ale poki co niech bedzie--%>
            <td colspan="2">
                <form:hidden path="id"/>
            </td>
        </tr>
        <tr>
            <td>
                <form:label path="dateW">
                    <spring:message code="workoutstats.form.dateW"/>
                </form:label>
            </td>
            <td>
                <form:input type="date" path="dateW"/>
            </td>
        </tr>
        <tr>
            <td>
                <form:label path="typeOfExcercise">
                    <spring:message code="workout.form.excercise"/>
                </form:label>
            </td>
            <td>
                <form:select path="typeOfExcercise">

                    <form:option value="" label="-"/>
                    <form:options items="${types}" />
                    <%--itemValue="id" itemLabel="category"--%>
                </form:select>
            </td>
        </tr>
        <tr>
            <td>
                <spring:message code="workout.form.series"/>
            </td>
        </tr>
        <tr>
            <td>
                <form:select path="series">
                    <form:option value="1"/>
                </form:select>

                <form:label path="numberOfRepetitions"/>
                <form:input path="numberOfRepetitions" type="number"/>
            </td>
        </tr>
        <tr>
            <td>
                <form:select path="series2">
                    <form:option value="2"/>
                </form:select>

                <form:label path="numberOfRepetitions2"/>
                <form:input path="numberOfRepetitions2" type="number"/>
            </td>
        </tr>
        <tr>
            <td>
                <form:select path="series3">
                    <form:option value="3"/>
                </form:select>

                <form:label path="numberOfRepetitions3"/>
                <form:input path="numberOfRepetitions3" type="number"/>
            </td>
        </tr>

        <td colspan="2">
                <spring:message code="workout.form.submit.label" var="labelSubmit"/>
                <input id="submitButton" name="submit" type="submit" value="${labelSubmit}">
        </td>
    </table>

</form:form>
    </td>
<!-- tabelka z wynikami -->
    <td valign="top">
<h3><spring:message code="workoutstats.stats.title"/></h3>
<table>
    <tr>
        <th width="100"><spring:message code="workoutstats.stats.label.dateW"/></th>
        <th width="100"><spring:message code="workoutstats.stats.label.excercise"/></th>
        <th width="75"><spring:message code="workoutstats.stats.label.series"/></th>
        <th width="75"><spring:message code="workoutstats.stats.label.repetitions"/></th>
        <th width="50"><spring:message code="workoutstats.stats.label.delete"/></th>
    </tr>
    <c:choose>
        <%--"workouts" to lista odbytych treningow--%>
        <c:when test="${!empty workouts}">
            <c:forEach items="${workouts}" var="workout">
                <tr>
                    <td rowspan="3">${workout.dateW}</td>
                    <td rowspan="3">${workout.typeOfExcercise}</td>
                    <td>${workout.series}</td>
                    <td>${workout.numberOfRepetitions}</td>
                    <td rowspan="3">
                        <a href="<c:url value='/workout/delete/${workout.id}'/>">
                                <button id="deleteButton">x</button></a>
                    </td>
                </tr>
                <tr>
                    <td>${workout.series2}</td>
                    <td>${workout.numberOfRepetitions2}</td>
                </tr>
                <tr>
                    <td>${workout.series3}</td>
                    <td>${workout.numberOfRepetitions3}</td>
                </tr>
                <%--------%>
            </c:forEach>
        </c:when>
        <c:otherwise>
            <tr>
                <td colspan="5"><spring:message code="workoutstats.stats.message.empty"/></td>
            </tr>
        </c:otherwise>
    </c:choose>

</table>
    </td>
    </tr>
</table>
</div>
</body>
</html>
<%--if u want to run create new db with SQLworkbench - "workout"--%>
