package pl.sda.pawe.dto;

import org.hibernate.validator.constraints.NotEmpty;
import pl.sda.pawe.domain.entity.Workout;

import java.io.Serializable;
import java.util.Date;

//potrzebne?

public class WorkoutDTO implements Serializable{

    private static final long serialVersionUID = -7768130995108510273L;

    private Long workoutId;
    @NotEmpty
    private Date date;
    @NotEmpty
    private String typeOfExcercise;
    //hardcoded v, but just in case
    @NotEmpty
    private Long series;
    @NotEmpty
    private Long numberOfRepetitions;

    public WorkoutDTO() {
    }

    public WorkoutDTO(Workout workout) {
        this.workoutId = workout.getId();
        this.date = workout.getDateW();
        this.typeOfExcercise = workout.getTypeOfExcercise();
        this.numberOfRepetitions = workout.getNumberOfRepetitions();
    }

    public Long getWorkoutId() {
        return workoutId;
    }

    public void setWorkoutId(Long workoutId) {
        this.workoutId = workoutId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getTypeOfExcercise() {
        return typeOfExcercise;
    }

    public void setTypeOfExcercise(String typeOfExcercise) {
        this.typeOfExcercise = typeOfExcercise;
    }

    public Long getNumberOfRepetitions() {
        return numberOfRepetitions;
    }

    public void setNumberOfRepetitions(Long numberOfRepetitions) {
        this.numberOfRepetitions = numberOfRepetitions;
    }
}
