package pl.sda.pawe.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import pl.sda.pawe.domain.entity.Workout;
import pl.sda.pawe.domain.type.TypeOfExcercise;
import pl.sda.workoutstats.service.workout.command.WorkoutCommandService;
import pl.sda.workoutstats.service.workout.query.WorkoutQueryService;

import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Controller
public class MainController {

    private static final Logger LOGGER = LoggerFactory.getLogger(MainController.class);

    private final MessageSource messageSource;
    private final WorkoutCommandService workoutCommandService;
    private final WorkoutQueryService workoutQueryService;

    @Autowired
    public MainController(MessageSource messageSource, WorkoutCommandService workoutCommandService, WorkoutQueryService workoutQueryService) {
        this.messageSource = messageSource;
        this.workoutCommandService = workoutCommandService;
        this.workoutQueryService = workoutQueryService;
    }

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        CustomDateEditor editor = new CustomDateEditor(dateFormat, true);
        binder.registerCustomEditor(Date.class, editor);
    }

    @RequestMapping(value = "/main", method = RequestMethod.GET)
    public String index(@ModelAttribute("flashWorkout") Workout workout,
                        Model model,
                        Locale locale) {
        LOGGER.debug("is executed!");

        model.addAttribute("workouts", workoutQueryService.findAll());
        model.addAttribute("workout", workout);

        List<String> types = Arrays.stream(TypeOfExcercise.values())
                .map(e -> messageSource.getMessage(e.getMessage(), new String[]{}, locale))
                .collect(Collectors.toList());
        model.addAttribute("types",types );

        return "index";
    }

    @RequestMapping(value = "/workout/save", method = RequestMethod.POST)
    public String saveStats(@Valid @ModelAttribute("workout") Workout workout,
                            BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        LOGGER.debug("is executed!");

        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.workout", bindingResult);
            redirectAttributes.addFlashAttribute("flashWorkout", workout);

            return "redirect:/main";
        }

        if(workout.getId() == null){
            workoutCommandService.create(workout);
        } else {
            workoutCommandService.update(workout);
        }

        return "redirect:/main";
    }
    @RequestMapping("/workout/delete/{id}")
    public String deleteWorkout(@PathVariable("id") Long id){
        LOGGER.debug("is executed!");
        workoutCommandService.delete(id);

        return "redirect:/main";
    }
}
