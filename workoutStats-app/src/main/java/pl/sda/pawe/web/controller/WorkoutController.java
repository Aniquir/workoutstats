package pl.sda.pawe.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;
import pl.sda.pawe.domain.entity.Workout;
import pl.sda.workoutstats.service.workout.command.WorkoutCommandService;
import pl.sda.workoutstats.service.workout.exception.WorkoutAlreadyExistsException;
import pl.sda.workoutstats.service.workout.exception.WorkoutNotFoundException;
import pl.sda.workoutstats.service.workout.query.WorkoutQueryService;

import java.util.List;

@RestController
public class WorkoutController {

    private static final Logger LOGGER = LoggerFactory.getLogger(WorkoutController.class);

    private final WorkoutCommandService workoutCommandService;
    private final WorkoutQueryService workoutQueryService;

    @Autowired
    public WorkoutController(WorkoutCommandService workoutCommandService, WorkoutQueryService workoutQueryService) {
        this.workoutCommandService = workoutCommandService;
        this.workoutQueryService = workoutQueryService;
    }

    @RequestMapping(value = "/workout/", method = RequestMethod.GET)
    public ResponseEntity<List<Workout>> getAll(){
        LOGGER.debug("is executed!");
        List<Workout> workouts = workoutQueryService.findAll();
        if (workouts.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(workouts, HttpStatus.OK);
    }
    @RequestMapping (value = "/workout/", method = RequestMethod.POST)
    public ResponseEntity<Void> create(@RequestBody Workout workout, UriComponentsBuilder ucBuilder){
        LOGGER.debug("is executed!");

        try {
            Long id = workoutCommandService.create(workout);
            HttpHeaders headers = new HttpHeaders();
            headers.setLocation(ucBuilder.path("/workout/{id}").buildAndExpand(id).toUri());

            return new ResponseEntity<>(headers, HttpStatus.CREATED);
        } catch (WorkoutAlreadyExistsException e){
                return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
    }
    @RequestMapping(value = "/workout/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Void> update(@PathVariable("id") Long id, @RequestBody Workout workout) {
        LOGGER.debug("is executed!");
        try {
            workoutCommandService.update(workout);

            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (WorkoutNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
    @RequestMapping(value = "/workout/{id}", method = RequestMethod.GET)
    public ResponseEntity<Workout> get(@PathVariable("id") Long id) {
        LOGGER.debug("is executed!");
        try {
            Workout workout = workoutQueryService.findById(id);

            return new ResponseEntity<>(workout, HttpStatus.OK);
        } catch (WorkoutNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
    @RequestMapping(value = "/workout/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> delete(@PathVariable("id") Long id){
        LOGGER.debug("is executed!");
        try {
            workoutCommandService.delete(id);

            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (WorkoutNotFoundException e){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
