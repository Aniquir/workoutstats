package pl.sda.pawe.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.ClassPathResource;

@Configuration
@Import(PersistanceConfig.class)
@ComponentScan(basePackages = {
        "pl.sda.pawe.workoutstats.jdbc.dao",
        "pl.sda.pawe.workoutstats.hibernate.dao",
        "pl.sda.workoutstats.service"})
public class AppConfig {

    @Bean
    public static  PropertySourcesPlaceholderConfigurer placeholderConfigurer(){
        PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer = new PropertySourcesPlaceholderConfigurer();
        propertySourcesPlaceholderConfigurer.setLocation(new ClassPathResource("persistence.properties"));

        return propertySourcesPlaceholderConfigurer;
    }

}
