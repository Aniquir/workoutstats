package pl.sda.pawe.domain.type;

public enum TypeOfExcercise {

    EXCERCISETummy("typeOfExcercise.tummy"),
    EXCERCISEObliqueTummy("typeOfExcercise.obliqueTummy"),
    EXCERCISEVerticalShears("typeOfExcercise.verticalShears"),
    EXCERCISEHorizontalShears("typeOfExcercise.horizontalShears"),
    EXCERCISEHalfSquat("typeOfExcercise.halfSquat"),
    EXCERCISEHandlePullUp("typeOfExcercise.handlePullUp"),
    EXCERCISEHandlePullDown("typeOfExcercise.handlePullDown"),
    EXCERCISEPump("typeOfExcercise.pump"),
    EXCERCISESquat("typeOfExcercise.squat"),
    EXCERCISELegsRaised("typeOfExcercise.legsRaised");

    private final String message;

    TypeOfExcercise(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
