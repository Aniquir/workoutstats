package pl.sda.pawe.domain.entity;

import org.hibernate.envers.Audited;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "WORKOUT")
@Audited
public class Workout implements Serializable{
    //zeby dodac serialVersionUID trzeba File>Settings>
    // inspections>Serialization issues>Serializable class without
    // 'serialVersionUID' i zaznaczyc
    private static final long serialVersionUID = 2672713426917418271L;

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    @Column(name = "WORKOUT_ID")
    private Long id;

    @Version
    @Column (name = "LATEST_VERSION")
    private Long version;

    @NotNull
    @Column (name = "DATEW")
    private Date dateW;

    @NotEmpty
    @Column(name = "TYPE_OF_EXCERCISE")
    private String typeOfExcercise;

    @Column (name = "SERIES")
    private Long series;

    @Column (name = "SERIES2")
    private Long series2;

    @Column (name = "SERIES3")
    private Long series3;

    @Column(name = "NUMBER_OF_REPETITIONS")
    private Long numberOfRepetitions;

    @Column(name = "NUMBER_OF_REPETITIONS2")
    private Long numberOfRepetitions2;

    @Column(name = "NUMBER_OF_REPETITIONS3")
    private Long numberOfRepetitions3;

    public Workout() {
    }

    public Workout(Date dateW, String typeOfExcercise, Long series,
                   Long series2, Long series3, Long numberOfRepetitions,
                   Long numberOfRepetitions2, Long numberOfRepetitions3) {
        this.dateW = dateW;
        this.typeOfExcercise = typeOfExcercise;
        this.series = series;
        this.series2 = series2;
        this.series3 = series3;
        this.numberOfRepetitions = numberOfRepetitions;
        this.numberOfRepetitions2 = numberOfRepetitions2;
        this.numberOfRepetitions3 = numberOfRepetitions3;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public Date getDateW() {
        return dateW;
    }

    public void setDateW(Date dateW) {
        this.dateW = dateW;
    }

    public String getTypeOfExcercise() {
        return typeOfExcercise;
    }

    public void setTypeOfExcercise(String typeOfExcercise) {
        this.typeOfExcercise = typeOfExcercise;
    }

    public Long getSeries() {
        return series;
    }

    public void setSeries(Long series) {
        this.series = series;
    }

    public Long getSeries2() {
        return series2;
    }

    public void setSeries2(Long series2) {
        this.series2 = series2;
    }

    public Long getSeries3() {
        return series3;
    }

    public void setSeries3(Long series3) {
        this.series3 = series3;
    }

    public Long getNumberOfRepetitions() {
        return numberOfRepetitions;
    }

    public void setNumberOfRepetitions(Long numberOfRepetitions) {
        this.numberOfRepetitions = numberOfRepetitions;
    }

    public Long getNumberOfRepetitions2() {
        return numberOfRepetitions2;
    }

    public void setNumberOfRepetitions2(Long numberOfRepetitions2) {
        this.numberOfRepetitions2 = numberOfRepetitions2;
    }

    public Long getNumberOfRepetitions3() {
        return numberOfRepetitions3;
    }

    public void setNumberOfRepetitions3(Long numberOfRepetitions3) {
        this.numberOfRepetitions3 = numberOfRepetitions3;
    }

    @Override
    public String toString() {
        return "Workout{" +
                "id=" + id +
                ", version=" + version +
                ", dateW=" + dateW +
                ", typeOfExcercise='" + typeOfExcercise + '\'' +
                ", series=" + series +
                ", series2=" + series2 +
                ", series3=" + series3 +
                ", numberOfRepetitions=" + numberOfRepetitions +
                ", numberOfRepetitions2=" + numberOfRepetitions2 +
                ", numberOfRepetitions3=" + numberOfRepetitions3 +
                '}';
    }

    //rodzaj cwiczenia(type od excercise),
    // serie(series),
    // ilosc powtorzen(number of repetitions)

//dodac ukryty id do workouta, ktory bedzie dostosowany do kazdej encji

}
