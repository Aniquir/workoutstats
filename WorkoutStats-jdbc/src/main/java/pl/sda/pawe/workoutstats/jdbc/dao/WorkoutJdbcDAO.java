package pl.sda.pawe.workoutstats.jdbc.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import pl.sda.pawe.domain.entity.Workout;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class WorkoutJdbcDAO {

    //nazewnictwo byc moze bedzie wymagalo sprawdzenia, szczegolnie workout/workoutstats (?)
    //poki co dodano WORKOUT_ID
    private static final String SQL_FIND_ALL = "SELECT WORKOUT_ID, DATEW, TYPE_OF_EXCERCISE, SERIES, SERIES2, SERIES3, NUMBER_OF_REPETITIONS, NUMBER_OF_REPETITIONS2, NUMBER_OF_REPETITIONS3 FROM WORKOUT";
    private static final String SQL_FIND_BY_ID = "SELECT WORKOUT_ID, DATEW, TYPE_OF_EXCERCISE, SERIES, SERIES2, SERIES3, NUMBER_OF_REPETITIONS, NUMBER_OF_REPETITIONS2, NUMBER_OF_REPETITIONS3 FROM WORKOUT WHERE WORKOUT_ID = ?";

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public WorkoutJdbcDAO(DataSource dataSource){
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public List<Workout> findAll() {
        return this.jdbcTemplate.query(SQL_FIND_ALL,
                new RowMapper<Workout>() {
                    public Workout mapRow(ResultSet rs, int rowNum) throws SQLException {
                        return mapToWorkout(rs);
                    }
                });
    }

    private Workout mapToWorkout(ResultSet resultSet) throws SQLException {

        Workout workout = new Workout();

        workout.setId(resultSet.getLong("WORKOUT_ID"));
        workout.setDateW(resultSet.getDate("DATEW"));
        workout.setTypeOfExcercise(resultSet.getString("TYPE_OF_EXCERCISE"));
        workout.setSeries(resultSet.getLong("SERIES"));
        workout.setSeries2(resultSet.getLong("SERIES2"));
        workout.setSeries3(resultSet.getLong("SERIES3"));
        workout.setNumberOfRepetitions(resultSet.getLong("NUMBER_OF_REPETITIONS"));
        workout.setNumberOfRepetitions2(resultSet.getLong("NUMBER_OF_REPETITIONS2"));
        workout.setNumberOfRepetitions3(resultSet.getLong("NUMBER_OF_REPETITIONS3"));

        return workout;
    }

    public Workout findById(Long id) {
        return this.jdbcTemplate.queryForObject(SQL_FIND_BY_ID, new Object[]{id},
                new RowMapper<Workout>() {
                    public Workout mapRow(ResultSet rs, int rowNum) throws SQLException {
                        return mapToWorkout(rs);
                    }
                });
    }

}
